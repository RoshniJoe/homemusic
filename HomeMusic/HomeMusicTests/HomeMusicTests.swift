//
//  HomeMusicTests.swift
//  HomeMusicTests
//
//  Created by Roshni Varghese on 2020-05-24.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import XCTest
@testable import HomeMusic

class HomeMusicTests: XCTestCase {
    
    func testSearchUsingSignaling() {
        
        APIController.artist = "Home"
        
        let didFinish = expectation(description: #function)
        var result: Music?
        
        let viewModel = MusicListViewModel()

        viewModel.fetchMusicList(completionHandler: { (music, error) in
            result = music
            didFinish.fulfill()
        })
        
        wait(for: [didFinish], timeout: 5)
        
        XCTAssertNotNil(result?.results?[0])
    }

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
