//
//  AlbumTableViewCell.swift
//  HomeMusic
//
//  Created by Roshni Varghese on 2020-05-23.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit

//This is a custom UITableViewCell class, which is reused to show the different items in the list
class AlbumTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageArtwork: UIImageView!
    @IBOutlet weak var labelTrackName: UILabel!
    @IBOutlet weak var labelCollectionName: UILabel!
    @IBOutlet weak var labelArtistName: UILabel!
    @IBOutlet weak var labelTrackPriceAndCurrency: UILabel!
    @IBOutlet weak var labelTrackTimeMillis: CustomTimeLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        labelTrackTimeMillis.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

