//
//  Music.swift
//  HomeMusic
//
//  Created by Roshni Varghese on 2020-05-23.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import Foundation
struct Music : Codable {
	let resultCount : Int?
	let results : [Results]?

	enum CodingKeys: String, CodingKey {

		case resultCount = "resultCount"
		case results = "results"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		resultCount = try values.decodeIfPresent(Int.self, forKey: .resultCount)
		results = try values.decodeIfPresent([Results].self, forKey: .results)
	}

}
