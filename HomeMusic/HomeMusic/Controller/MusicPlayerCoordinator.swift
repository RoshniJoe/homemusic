//
//  MusicPlayerCoordinator.swift
//  HomeMusic
//
//  Created by Roshni Varghese on 2020-05-23.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit

// This Coordinator object (of type 'Class') coordinates the App's navigation.
class MusicPlayerCoordinator : NSObject {
    
    //Setting up the navigation controller for the 2 page application.
    private(set) lazy var rootViewController: UINavigationController = {
        let controller = UINavigationController(rootViewController: musicListTableViewController)
        controller.navigationBar.barTintColor = UIColor(named: "HomeOrange")
        controller.navigationBar.tintColor = UIColor.white
        return controller
    }()
    
    //Setting up the rootview for the application.
    private lazy var musicListTableViewController : MusicListTableViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "MusicListTableViewController") as! MusicListTableViewController
        controller.delegate = self
        return controller
    }()

    //Setting up a detail page to naviagte to, in order to show further information from the selection done on the first page of the application.
    func showMediaPlayerViewController(mediaUrl: String, animated : Bool = true){
        let controller = MediaPlayerViewController()
        controller.mediaUrl = mediaUrl
        controller.delegate = self
        rootViewController.pushViewController(controller, animated: true)
    }
    
    //Navigate back to the rootview
    func popToPreviousViewController(controller: UIViewController, animated: Bool) {
        controller.navigationController?.popViewController(animated: animated)
    }
}

extension MusicPlayerCoordinator: MusicListTableViewControllerDelegate{
    //Delegate to carry and show relevant data from the initial page
    func  redirectToMediaPlayerViewController(with playUrl: String) {
        showMediaPlayerViewController(mediaUrl: playUrl, animated: true)
    }
}

extension MusicPlayerCoordinator: MediaPlayerViewControllerDelegate{
    //Delegate to pop back to the previous page
    func popBackToMusicListTableViewController(controller: UIViewController, animated: Bool = true) {
        popToPreviousViewController(controller: controller, animated: true)
    }
}
