//
//  MediaPlayerViewController.swift
//  HomeMusic
//
//  Created by Roshni Varghese on 2020-05-23.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit
import AVKit

//Protocol to coordinate the navigation to pop back to the previous page.
protocol MediaPlayerViewControllerDelegate {
    func popBackToMusicListTableViewController(controller: UIViewController, animated: Bool)
}

//This class plays the audio/video url as selected from the list in the previous page.
class MediaPlayerViewController: UIViewController {
    
    //MARK: - Declarations
    
    var mediaUrl: String?
    var videoUrl: URL?
    var player: AVPlayer?
    let avPVC = AVPlayerViewController()
    
    var delegate: MediaPlayerViewControllerDelegate?
    
    //MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupMediaPlayer()
    }
    
    //MARK: - Setup Media Player
    
    func setupMediaPlayer(){
        
        guard let videoUrl = URL(string: mediaUrl ?? "") else { return }
        
        self.player = AVPlayer(url: videoUrl)
        avPVC.player = self.player
        avPVC.view.frame = self.view.bounds
        self.addChild(avPVC)
        self.view.addSubview(avPVC.view)
        avPVC.didMove(toParent: self)
    }
}
