//
//  ViewController.swift
//  HomeMusic
//
//  Created by Roshni Varghese on 2020-05-23.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit
import Alamofire

//Protocol to coordinate the navigation to the show the details page, from the selection done on the initial page.
protocol MusicListTableViewControllerDelegate {
    func redirectToMediaPlayerViewController(with playUrl: String)
}

//This class lists all the data from the iTunes Search API according to the keyword searched.
class MusicListTableViewController: UITableViewController {
    
    //MARK: - Declarations
    
    let viewModel = MusicListViewModel()
    var musicList: Music?
    var delegate: MusicListTableViewControllerDelegate?
    var messageLabel = UILabel()
    var cancelButtonTapped = false
    
    let searchController = UISearchController(searchResultsController: nil)
    
    //MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = " iTunes"
        setupSearchView()
        setupTableViewGradient()
    }
    
    //MARK: - Setup UI
    
    //To enable the user to search for any string in the iTunes search API.
    func setupSearchView(){
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search here"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    //Display a placeholder label in case of an empty list.
    func setupPlaceholderForEmptyTableView(message: String){
        let rect = CGRect(origin: CGPoint(x: 20,y :0), size: CGSize(width: self.view.bounds.size.width - 40, height: self.view.bounds.size.height))
        messageLabel.frame = rect
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Chalkboard SE", size: 20)
        self.tableView.backgroundView?.addSubview(messageLabel)
        self.tableView.separatorStyle = .none
    }
    
    //Set a gradient to the tableView background
    func setupTableViewGradient(){
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(named: "HomeOrange")?.cgColor ?? UIColor.white, UIColor.white.cgColor]
        gradientLayer.frame = tableView.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        let backgroundView = UIView(frame: self.tableView.bounds)
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        tableView.backgroundView = backgroundView
    }
    //MARK: - Table View Datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if (self.musicList?.resultCount ?? 0) > 0 {
            messageLabel.isHidden = true
            return 1
        } else {
            //Show a placeholder label in case of an empty list.
            messageLabel.isHidden = false
            self.setupPlaceholderForEmptyTableView(message: "Please enter a valid text to start searching...")
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.musicList?.resultCount ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumCell", for: indexPath) as! AlbumTableViewCell
        
        //Retrieve data from model class in order to populate the tableview cells.
        let trackDetail : Results = (self.musicList?.results?[indexPath.row])!
        
        //Assign data to UI elements inside the cell.
        Alamofire.request(URL(string: trackDetail.artworkUrl100 ?? "")!, method: .get)
            .validate()
            .responseData(completionHandler: { (responseData) in
                DispatchQueue.main.async {
                    cell.imageArtwork.image = UIImage(data: responseData.data!)
                }
            })
        cell.labelTrackName.text = trackDetail.trackName ?? ""
        cell.labelCollectionName.text = trackDetail.collectionName ?? ""
        cell.labelArtistName.text = trackDetail.artistName ?? "Unknown Artist"
        cell.labelTrackPriceAndCurrency.text = "$\(trackDetail.trackPrice ?? 0.0) \(trackDetail.currency ?? "")"
        
        //Show duration of the audio/video track in the format : '12:09:12'
        let (hours,minutes,seconds) = viewModel.millisecondsToHoursMinutesSeconds(milliseconds: trackDetail.trackTimeMillis ?? 0)
        if hours > 1{
            cell.labelTrackTimeMillis.text = String(format: "%02i:%02i:%02i", hours, minutes, seconds)
        } else{
            cell.labelTrackTimeMillis.text = String(format: "%02i:%02i", minutes, seconds)
        }
        
        return cell
    }
    
    //MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Redirect to MediaPlayerViewController to show the preview of audio/video file selected from the main list.
        self.delegate?.redirectToMediaPlayerViewController(with: self.musicList?.results?[indexPath.row].previewUrl ?? "")
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
    //MARK: - Reload Table View After fetching data from API
    
    func reloadPlaylist(){
        //Check for network availability.
        if NetworkReachabilityManager()?.isReachable ?? false {
            APIController.hud.show(in: self.view)
            viewModel.fetchMusicList { (music, error) in
                self.musicList = music
                self.tableView.reloadData()
                self.cancelButtonTapped = false
            }
        }else {
            //Show alert in case of network outage.
            let alertController = UIAlertController(title: "Network unavailable!", message: "Please check your internet connection.", preferredStyle: .alert)
            let action = UIAlertAction(title: "Try again", style: .default) { (action:UIAlertAction) in
                self.reloadPlaylist()
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

//MARK: - Search Controller Delegates

extension MusicListTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        //To retain the search list without resetting to empty
        if !cancelButtonTapped {
            APIController.artist = searchBar.text!.replacingOccurrences(of: " ", with: "")
            reloadPlaylist() // reloads the playlist table on any change in the search field.
        }
    }
}

extension MusicListTableViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelButtonTapped = true
        APIController.artist = searchBar.text!.replacingOccurrences(of: " ", with: "")
        reloadPlaylist() // reloads the playlist table on tapping 'Cancel' near the search field.
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.text = APIController.artist //To prepopulate the search field with the previous value it held.
    }
}
