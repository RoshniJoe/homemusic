//
//  APIController.swift
//  HomeMusic
//
//  Created by Roshni Varghese on 2020-05-23.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import Foundation
import Alamofire
import JGProgressHUD

class APIController {
    
    //MARK: - Declarations
    
    static let hud = JGProgressHUD(style: .dark) // To show the loading state while data is being fetched.
    
    static let shared = APIController()
    
    //MARK: - base URL and search term
    
    static let domain = "https://itunes.apple.com/search?term="
    static var artist : String? // To hold the dynamic <search_term>
        
    //MARK: - API call
    
    func request(url: URL,parameters : Parameters, completion: @escaping (Any?, Error?) -> Void) {
        
        Alamofire.request(url, method: .get
            , parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in
                completion(response.result.value, response.error)
        }
    }
}
