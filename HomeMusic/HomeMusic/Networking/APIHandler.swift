//
//  APIHandler.swift
//  HomeMusic
//
//  Created by Roshni Varghese on 2020-05-23.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum Result<S, E> {
    case success(S)
    case failure(E)
}

class APIHandler: NSObject {
    
    static let sharedClient = APIHandler()
    
    //    MARK: - make API request
    
    func makeRequest<T: Codable>(withType type: HTTPMethod, path: String, params: Parameters, returnType: T.Type, andCallBack callback: @escaping (Result<T, String>) -> Void){
        
        Alamofire.request(path, method: type, parameters: params)
            .validate(statusCode: 200..<500)
            .responseJSON {
                response in
                switch response.result {
                case .success(let data):
                    do {
                        let jsonData = try? JSONSerialization.data(withJSONObject:data)
                        let object = try JSONDecoder().decode( returnType , from: jsonData!)
                        callback(Result.success(object))
                    } catch {
                        callback(Result.failure(error.localizedDescription))
                    }
                case .failure(let error):
                    callback(Result.failure(error.localizedDescription))
                }
        }
        
    }
}
