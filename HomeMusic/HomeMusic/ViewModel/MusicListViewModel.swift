//
//  MusicListViewModel.swift
//  HomeMusic
//
//  Created by Roshni Varghese on 2020-05-23.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit

class MusicListViewModel {
        
    // Function to coordinate the fetching of data from API and saving it in the model.
    func fetchMusicList(completionHandler: @escaping (_ result: Music?, _ error: String) -> Void){
        APIHandler.sharedClient.makeRequest(withType: .get, path: APIController.domain + (APIController.artist ?? ""), params: [:], returnType: Music.self) { (result) in
             APIController.hud.dismiss()
            switch(result) {
            case .success(let user):
                completionHandler(user, "")
            case .failure( _):
                completionHandler(nil,"Error")
            }
        }
    }
    
    // Function to calculate hours, minutes and seconds from milliseconds provided.
    func millisecondsToHoursMinutesSeconds (milliseconds : Int) -> (Int, Int, Int) {
        let seconds = milliseconds / 1000
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
